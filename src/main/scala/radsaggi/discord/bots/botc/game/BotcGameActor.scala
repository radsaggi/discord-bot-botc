package radsaggi.discord.bots.botc.game

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.ActorContext
import akka.actor.typed.scaladsl.Behaviors
import radsaggi.discord.bots.botc.config.BotcBotConfig
import radsaggi.discord.bots.botc.game.conversations.GameConversationsActor
import radsaggi.discord.bots.botc.game.conversations.NominationsActor
import radsaggi.discord.bots.botc.game.receptionist.GameReceptionistActor
import radsaggi.discord.bots.botc.game.setup.GameSetupActor
import radsaggi.discord.bots.botc.game.setup.GameSetupActor.GameSetupCompletedEvent
import radsaggi.discord.bots.botc.jdaakka.BotcJdaWrapper
import radsaggi.discord.bots.botc.model._

import scala.util.Failure
import scala.util.Success

object BotcGameActor {

  def apply(
      command: TextChannelCommand.NewGame,
      config: BotcBotConfig,
      jdaWrapper: BotcJdaWrapper): Behavior[ExternalCommand] =
    Behaviors
      .setup[AnyRef](context => new BotcGameActor(context, config, jdaWrapper).start(command))
      .narrow[ExternalCommand]
}

final class BotcGameActor private (
    context: ActorContext[AnyRef],
    config: BotcBotConfig,
    jdaWrapper: BotcJdaWrapper) {
  context.log.info("Starting BotcGameActor for a new Game")

  private val gameConfig = config.game

  private val messageActor = context.self.narrow[ExternalMessage]
  private val receptionist =
    context.spawn(GameReceptionistActor(), "GameReceptionist")

  def start(command: TextChannelCommand.NewGame): Behavior[AnyRef] = {
    case class InitComplete(channelCategory: ChannelCategory, voiceChannel: VoiceChannel)
    case object Retry

    /** Helper method that creates the GameData.Init information */
    def newInit(voiceChannel: VoiceChannel, category: ChannelCategory): GameData.Init =
      GameData.Init(
        storyteller = command.metadata.sender,
        numPlayers = command.numPlayers,
        gameConfig = config.game,
        channels = GameData.Channels(
          publicText = command.metadata.channel,
          publicConversations = voiceChannel,
          category = category),
      )

    val future = jdaWrapper.run { implicit exec =>
      val channelCategoryFuture =
        jdaWrapper
          .createParentCategory(command.metadata.channel, gameConfig.channels.categoryName)
      val voiceChannelFuture = channelCategoryFuture.flatMap(
        jdaWrapper.openVoiceChannel(_, gameConfig.channels.globalConversationsName))

      channelCategoryFuture.zip(voiceChannelFuture)
    }
    context.pipeToSelf(future) {
      case Success((channelCategory, voiceChannel)) => InitComplete(channelCategory, voiceChannel)
      case Failure(exception) =>
        context.log.error("Exception trying to create channel category", exception)
        Retry
    }

    receiveMessage {
      case InitComplete(channelCategory, voiceChannel) =>
        initComplete(newInit(voiceChannel, channelCategory))
      case Retry =>
        start(command)
    }
  }

  def initComplete(initData: GameData.Init): Behavior[AnyRef] = {
    context.log.info("BotcGameActor init complete. GameInit: {}", initData)
    val setupCompletedActor = context.self.narrow[GameSetupCompletedEvent]
    context.spawn(
      GameSetupActor(initData, receptionist, messageActor, setupCompletedActor),
      "GameSetupActor")

    receiveMessage {
      case GameSetupCompletedEvent(allocations, seatingOrder) =>
        setupComplete(GameData.Setup(initData, allocations, seatingOrder))
    }
  }

  def setupComplete(setupData: GameData.Setup): Behavior[AnyRef] = {
    context.spawn(
      GameConversationsActor(setupData, jdaWrapper, jdaWrapper, receptionist, messageActor),
      "GameConversationsActor")
    context.spawn(NominationsActor(setupData, messageActor, receptionist), "NominationsActor")

    receiveMessage(PartialFunction.empty)
  }

  /** Helper method that handles all ExternalCommands and External messages. */
  private def receiveMessage(handler: PartialFunction[AnyRef, Behavior[AnyRef]]): Behavior[AnyRef] =
    Behaviors.receiveMessage(handler.orElse {
      case command: ExternalCommand =>
        receptionist ! GameReceptionistActor.Deliver(command)
        Behaviors.same

      case message: ExternalMessage =>
        // TODO: Add verification for outgoing messages
        jdaWrapper.sendMessage(message)
        Behaviors.same

      case _ => Behaviors.unhandled
    })
}
