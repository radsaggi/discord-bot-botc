package radsaggi.discord.bots.botc.game.conversations

import akka.actor.typed.ActorRef
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.ActorContext
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.scaladsl.TimerScheduler
import radsaggi.discord.bots.botc.game.receptionist.GameReceptionistActor.GameReceptionist
import radsaggi.discord.bots.botc.game.receptionist.GameReceptionistActor.Register
import radsaggi.discord.bots.botc.jdaakka.VoiceChannelsWrapper
import radsaggi.discord.bots.botc.jdaakka.WithExecutionContext
import radsaggi.discord.bots.botc.model.TextChannelCommand.DisableConversations
import radsaggi.discord.bots.botc.model.TextChannelCommand.EnableConversations
import radsaggi.discord.bots.botc.model.TextChannelCommand.TextChannelMeta
import radsaggi.discord.bots.botc.model._

import scala.concurrent.Future
import scala.util.Failure
import scala.util.Random
import scala.util.Success

object GameConversationsActor {
  sealed trait Command
  case class WrappedEnable(command: TextChannelCommand.EnableConversations) extends Command
  case class WrappedDisable(command: TextChannelCommand.DisableConversations) extends Command
  private case object DisableDelayReached extends Command
  private case class EnableComplete(voiceChannels: Seq[VoiceChannel]) extends Command
  private case object DisableComplete extends Command
  private case class FutureError(exception: Throwable) extends Command

  private object DisableTimerKey

  private object Messages {
    val notStoryteller        = "Only Storyteller is allowed to do that!"
    val channelsAlreadyActive = "Conversations channels are already active."
    val channelsNotActive     = "Conversation channels are not active."

    def errorOccurred(exception: Throwable) = "Error occurred: " + exception.getMessage
  }

  def apply(
      setupData: GameData.Setup,
      jdaWrapper: VoiceChannelsWrapper,
      withEC: WithExecutionContext,
      receptionist: GameReceptionist,
      messageActor: ActorRef[ExternalMessage]): Behavior[Command] =
    Behaviors.setup(ctx =>
      Behaviors.withTimers(timer =>
        new GameConversationsActor(ctx, timer, setupData, jdaWrapper, withEC, messageActor)
          .start(receptionist)))
}

import radsaggi.discord.bots.botc.game.conversations.GameConversationsActor._

final class GameConversationsActor private (
    context: ActorContext[Command],
    timer: TimerScheduler[Command],
    setupData: GameData.Setup,
    jdaWrapper: VoiceChannelsWrapper,
    withExecutionContext: WithExecutionContext,
    messageActor: ActorRef[ExternalMessage]) {
  context.log.debug("Starting GameConversationsActor")

  def start(receptionist: GameReceptionist): Behavior[Command] = {
    val enableConversationsActor = context.messageAdapter(WrappedEnable.apply)
    receptionist ! Register[TextChannelCommand.EnableConversations](enableConversationsActor)

    val disableConversationsActor = context.messageAdapter(WrappedDisable.apply)
    receptionist ! Register[TextChannelCommand.DisableConversations](disableConversationsActor)

    channelsDeleted()
  }

  def channelNames(count: Int): Seq[String] = {
    val names = Random.shuffle(setupData.gameConfig.channels.conversationsNames)
    if (names.size >= count) {
      names.take(count)
    } else {
      val iterations = (count / names.size) + 1
      LazyList.tabulate(iterations)(n => names.map(_ + s" #${n + 1}")).flatten.take(count)
    }
  }

  def channelsDeleted(): Behavior[Command] =
    Behaviors.receiveMessage {
      case WrappedEnable(EnableConversations(numChannels, metadata)) =>
        storyTellerAuthz(metadata) {
          createVoiceChannels(channelNames(numChannels))
          Behaviors.same
        }

      case WrappedDisable(DisableConversations(_, metadata)) =>
        storyTellerAuthz(metadata) {
          messageActor ! SendTextChannelMessage(Messages.channelsNotActive, metadata.channel)
          Behaviors.same
        }

      case EnableComplete(voiceChannels) =>
        context.log.debug("Creating conversations channels completed successfully!")
        channelsCreated(voiceChannels)

      case FutureError(error) =>
        context.log.error("Exception encountered trying to create conversation channels", error)
        messageActor ! SendPrivateChatMessage(Messages.errorOccurred(error), setupData.storyteller)
        Behaviors.same

      case DisableDelayReached =>
        Behaviors.same

      case DisableComplete =>
        Behaviors.same
    }

  def channelsCreated(channels: Seq[VoiceChannel]): Behavior[Command] =
    Behaviors.receiveMessage {
      case WrappedEnable(EnableConversations(_, metadata)) =>
        storyTellerAuthz(metadata) {
          messageActor ! SendTextChannelMessage(Messages.channelsAlreadyActive, metadata.channel)
          Behaviors.same
        }

      case WrappedDisable(DisableConversations(timeout, metadata)) =>
        storyTellerAuthz(metadata) {
          timer.startSingleTimer(DisableTimerKey, DisableDelayReached, timeout)
          Behaviors.same
        }

      case DisableDelayReached =>
        deleteVoiceChannels(channels)
        Behaviors.same

      case DisableComplete =>
        context.log.debug("Disabling conversations channels completed successfully!")
        channelsDeleted()

      case FutureError(error) =>
        context.log.error("Exception encountered trying to create conversation channels", error)
        messageActor ! SendPrivateChatMessage(Messages.errorOccurred(error), setupData.storyteller)
        Behaviors.same

      case EnableComplete(_) =>
        Behaviors.same
    }

  private def createVoiceChannels(names: Seq[String]): Unit = {
    val future =
      withExecutionContext.run { implicit exec =>
        Future.traverse(names)(jdaWrapper.openVoiceChannel(setupData.channels.category, _))
      }
    context.pipeToSelf(future) {
      case Success(createdChannels) => EnableComplete(createdChannels)
      case Failure(exception)       => FutureError(exception)
    }
  }

  private def deleteVoiceChannels(channels: Seq[VoiceChannel]): Unit = {
    val future =
      withExecutionContext.run { implicit exec =>
        val usersToKick: Seq[DataTypes.User] = channels.flatMap(jdaWrapper.getMembersInVoiceChannel)
        val publicChannel: VoiceChannel      = setupData.channels.publicConversations
        Future
          .traverse(usersToKick)(jdaWrapper.moveUserToChannel(_, publicChannel))
          .flatMap(_ => Future.traverse(channels)(jdaWrapper.closeVoiceChannel))
      }
    context.pipeToSelf(future) {
      case Success(_)         => DisableComplete
      case Failure(exception) => FutureError(exception)
    }
  }

  @inline
  private def storyTellerAuthz(meta: TextChannelMeta)(
      exec: => Behavior[Command]): Behavior[Command] =
    if (meta.sender == setupData.storyteller) {
      exec
    } else {
      messageActor ! SendTextChannelMessage(Messages.notStoryteller, meta.channel)
      Behaviors.same
    }
}
