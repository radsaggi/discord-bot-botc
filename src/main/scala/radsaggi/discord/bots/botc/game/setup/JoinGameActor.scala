package radsaggi.discord.bots.botc.game.setup

import akka.actor.typed.ActorRef
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.ActorContext
import akka.actor.typed.scaladsl.Behaviors
import radsaggi.discord.bots.botc.model._

import scala.util.Random

object JoinGameActor {
  type Command = TextChannelCommand.JoinGame

  /** An InternalEvent fired when all the players have joined the game. */
  case class PlayersJoinedEvent(seatedPlayers: DataTypes.SeatedPlayers)

  private object Messages {
    val ready                               = "Ready to accept new players.!"
    def newlyJoined(player: DataTypes.User) = s"${player.printName} has now joined the game."
    def waitingFor(n: Int)                  = s"Waiting for $n players to join the game."
    def waitingForMore(n: Int)              = s"Waiting for $n more players to join the game."

    def alreadyJoined(player: DataTypes.User) =
      s"${player.printName} has already joined. Ignoring..."

    def allJoined(players: DataTypes.SeatedPlayers): String =
      s"All ${players.size} players have joined the game (in seating-order):\n" +
      players.map("> " + _.printName).mkString("\n")
  }

  def apply(
      initData: GameData.Init,
      messageActor: ActorRef[ExternalMessage],
      replyTo: ActorRef[PlayersJoinedEvent]): Behavior[Command] =
    Behaviors.setup(context =>
      new JoinGameActor(context, initData, messageActor, replyTo).receive(Seq.empty))
}

import radsaggi.discord.bots.botc.game.setup.JoinGameActor._

final class JoinGameActor private (
    context: ActorContext[Command],
    initData: GameData.Init,
    messageActor: ActorRef[ExternalMessage],
    replyTo: ActorRef[PlayersJoinedEvent]) {
  context.log.info("Starting JoinGameActor")
  messageActor ! SendPublicMessage(Messages.ready)

  def receive(players: Seq[DataTypes.User]): Behavior[Command] =
    if (players.size == initData.numPlayers) {
      val seatedPlayers = determineSeatingOrder(players)
      messageActor ! SendPublicMessage(Messages.allJoined(seatedPlayers))
      replyTo ! PlayersJoinedEvent(seatedPlayers)
      Behaviors.stopped
    } else {
      val message =
        if (players.isEmpty) Messages.waitingFor(initData.numPlayers)
        else Messages.waitingForMore(initData.numPlayers - players.size)
      messageActor ! SendPublicMessage(message)

      Behaviors.receiveMessage { command =>
        val player = command.metadata.sender

        if (!initData.gameConfig.allowUserMultipleJoins && players.contains(player)) {
          // Player has already joined.
          context.log.warn("Avoiding duplicate player from joining: {}", player)
          messageActor ! SendPublicMessage(Messages.alreadyJoined(player))
          Behaviors.same
        } else if (player == initData.storyteller) {
          context.log.warn("Avoiding storyteller from joining: {}", player)
          messageActor ! SendPublicMessage(Messages.alreadyJoined(player))
          Behaviors.same
        } else {
          messageActor ! SendPublicMessage(Messages.newlyJoined(player))
          receive(players :+ player)
        }
      }
    }

  @inline private def SendPublicMessage(message: DataTypes.Message): SendTextChannelMessage = {
    SendTextChannelMessage(message, initData.channels.publicText)
  }

  @inline
  private def determineSeatingOrder(players: Seq[DataTypes.User]) = Random.shuffle(players)
}
