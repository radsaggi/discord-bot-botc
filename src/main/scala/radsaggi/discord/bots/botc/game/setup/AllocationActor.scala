package radsaggi.discord.bots.botc.game.setup

import akka.actor.typed.ActorRef
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.ActorContext
import akka.actor.typed.scaladsl.Behaviors
import radsaggi.discord.bots.botc.model.DataTypes.Characters
import radsaggi.discord.bots.botc.model.DataTypes.SeatedPlayers
import radsaggi.discord.bots.botc.model._

import scala.collection.immutable.HashSet
import scala.util.Random

object AllocationActor {
  type Command = PrivateChatCommand.SelectCharacter

  /** An InternalEvent fired when all the players have received their character roles. */
  case class CharactersAllocatedEvent(allocation: DataTypes.Allocation)

  private object Messages {
    val ready =
      "Switch to private chat with \"Blood On The Clocktower\" bot for character allocation."

    val allAllocated         = "All characters have been allocated."
    def chooseNumber(n: Int) = s"Choose an integer number between 1 and $n inclusive."

    def alreadyAllocated(ch: DataTypes.Character)    = s"You have already been allocated $ch"
    def youAre(chosenCharacter: DataTypes.Character) = s"You are '$chosenCharacter'"
    def allocated(player: DataTypes.User)            = s"${player.printName} has received their role."

    def allocationInfo(players: DataTypes.SeatedPlayers, allocation: DataTypes.Allocation): String =
      s"Allocation Info for players (in seating-order):\n" +
      players.map(p => s"  ${p.printName} -> ${allocation(p)}").mkString("\n")
  }

  def apply(
      initData: GameData.Init,
      players: SeatedPlayers,
      characters: Characters,
      messageActor: ActorRef[ExternalMessage],
      replyTo: ActorRef[CharactersAllocatedEvent]): Behavior[Command] =
    Behaviors.setup(ctx =>
      new AllocationActor(ctx, initData, players, characters, messageActor, replyTo)
        .receive(Map.empty, HashSet(characters: _*)))
}

import radsaggi.discord.bots.botc.game.setup.AllocationActor._

final class AllocationActor private (
    context: ActorContext[Command],
    initData: GameData.Init,
    players: DataTypes.SeatedPlayers,
    characters: DataTypes.Characters,
    messageActor: ActorRef[ExternalMessage],
    replyTo: ActorRef[CharactersAllocatedEvent]) {
  context.log.info("Starting character AllocationActor")
  messageActor ! SendTextChannelMessage(Messages.ready, initData.channels.publicText)

  // Send message to each player.
  players.foreach { user =>
    messageActor ! SendPrivateChatMessage(Messages.chooseNumber(characters.size), user)
  }
  context.log.debug("Reminder chooseNumber message sent to every player.")

  def receive(
      allocations: Map[DataTypes.User, DataTypes.Character],
      remCharacters: Set[DataTypes.Character]): Behavior[Command] =
    if (allocations.size == initData.numPlayers) {
      context.log.info("All allocations complete")
      messageActor ! SendTextChannelMessage(Messages.allAllocated, initData.channels.publicText)
      messageActor ! SendPrivateChatMessage(
        Messages.allocationInfo(players, allocations),
        initData.storyteller)
      replyTo ! CharactersAllocatedEvent(allocations)
      Behaviors.stopped
    } else {
      Behaviors.receiveMessage { command =>
        val sender        = command.metadata.sender
        val senderChannel = command.metadata.channel

        if (!players.contains(sender)) { // AuthZ: Don't know who sent the command.
          context.log.warn("Ignoring command received from unknown player: {}", sender)
          Behaviors.same
        } else if (allocations.contains(sender)) { // Player already has been allocated a character.
          context.log.debug("Ignoring command received from unknown player: {}", sender)
          val character = allocations(sender)
          messageActor ! SendPrivateChatMessage(Messages.alreadyAllocated(character), senderChannel)
          Behaviors.same
        } else {
          val chosenCharacter = chooseCharacter(remCharacters, command.choice)
          context.log.debug("Allocated character to {}", sender)
          messageActor ! SendPrivateChatMessage(Messages.youAre(chosenCharacter), senderChannel)
          messageActor ! SendPrivateChatMessage(Messages.allocated(sender), initData.storyteller)
          receive(allocations + (sender -> chosenCharacter), remCharacters - chosenCharacter)
        }
      }
    }

  @inline
  private def chooseCharacter(
      characters: Set[DataTypes.Character],
      choice: Int): DataTypes.Character =
    characters.iterator.drop((choice + Random.nextInt(characters.size)) % characters.size).next()
}
