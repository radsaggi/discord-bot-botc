package radsaggi.discord.bots.botc.game.conversations

import akka.actor.typed.ActorRef
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.ActorContext
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.scaladsl.TimerScheduler
import radsaggi.discord.bots.botc.game.receptionist.GameReceptionistActor.GameReceptionist
import radsaggi.discord.bots.botc.game.receptionist.GameReceptionistActor.Register
import radsaggi.discord.bots.botc.game.receptionist.GameReceptionistActor.Unregister
import radsaggi.discord.bots.botc.model.DataTypes.Message
import radsaggi.discord.bots.botc.model.TextChannelCommand
import radsaggi.discord.bots.botc.model._

import scala.collection.immutable.HashSet
import scala.collection.immutable.Queue
import scala.concurrent.duration.FiniteDuration

object NominationsActor {

  sealed trait Command
  private case class WrappedOpenNominations(command: TextChannelCommand.OpenNominations)
      extends Command
  private case class WrappedCloseNominations(command: TextChannelCommand.CloseNominations)
      extends Command
  private case class WrappedNominate(command: TextChannelCommand.Nominate) extends Command
  private case object VotingComplete extends Command
  private case object CloseNominationsTimeout extends Command

  // Key for timer that closes nominations.
  private object CloseNominationsTimerKey

  private object Messages {
    val notStoryteller: Message    = "You are not storyteller. Only the storyteller may do this."
    val nominationsOpen: Message   = "Nominations are now open."
    val storytellerNom: Message    = "I do not understand it when storyteller nominates. :("
    val nominationsClosed: Message = "Nominations are now closed."
    val waitingForOpen: Message    = "I can't do that! Nominations are not open yet."
    val alreadyOpen: Message       = "Nominations are already open and in progress."
    val noMoreQueued: Message      = "There are no more queued nominations."

    def repeatNominee(player: DataTypes.Player): Message =
      s"${player.printName} has already been nominated and cannot be nominated again."

    def repeatNominator(player: DataTypes.Player): Message =
      s"${player.printName} has already nominated for the day and may not nominate again."

    def registered(nomination: DataTypes.Nomination): Message =
      s"Nomination registered. ${nomination._1.printName} has nominated ${nomination._2.printName}"

    def nextQueuedNomination(nomination: DataTypes.Nomination, remaining: Int): Message =
      s"Next nomination: ${DataTypes.toString(nomination)}; there are $remaining more after this."

    def closingSoon(timeout: FiniteDuration): Message = s"Closing nominations in $timeout..."

    def closedAndWaiting(n: Int): Message =
      s"Nominations have already been closed; waiting for last $n nominations to complete..."
  }

  def apply(
      gameSetup: GameData.Setup,
      messageActor: ActorRef[ExternalMessage],
      receptionist: GameReceptionist): Behavior[Command] =
    Behaviors.setup { ctx =>
      Behaviors.withTimers { timer =>
        new NominationsActor(ctx, timer, gameSetup, messageActor, receptionist).start()
      }
    }

  private val ChildVotingActorName = "VotingActor"
}

import radsaggi.discord.bots.botc.game.conversations.NominationsActor._

final class NominationsActor private (
    context: ActorContext[Command],
    timers: TimerScheduler[Command],
    gameSetup: GameData.Setup,
    messageActor: ActorRef[ExternalMessage],
    receptionist: GameReceptionist) {
  context.log.debug("Starting NominationsActor")

  private val nominateCommandActor = context.messageAdapter(WrappedNominate.apply)

  @inline private def SendPublicMessage(message: Message): SendTextChannelMessage = {
    SendTextChannelMessage(message, gameSetup.channels.publicText)
  }

  def start(): Behavior[Command] = {
    receptionist ! Register[TextChannelCommand.OpenNominations](
      context.messageAdapter(WrappedOpenNominations.apply))
    receptionist ! Register[TextChannelCommand.CloseNominations](
      context.messageAdapter(WrappedCloseNominations.apply))

    waitForOpenNominations()
  }

  private def waitForOpenNominations(): Behavior[Command] = {
    Behaviors.receiveMessage {
      case WrappedOpenNominations(TextChannelCommand.OpenNominations(metadata)) =>
        if (metadata.sender != gameSetup.storyteller) {
          messageActor ! SendPublicMessage(Messages.notStoryteller)
          Behaviors.same
        } else {
          receptionist ! Register[TextChannelCommand.Nominate](nominateCommandActor)
          messageActor ! SendPublicMessage(Messages.nominationsOpen)
          receiveNominations(
            nominators = HashSet.empty,
            nominees = HashSet.empty,
            queuedNominations = Queue.empty)
        }

      case ignored =>
        messageActor ! SendPublicMessage(Messages.waitingForOpen)
        context.log.warn("Ignoring command {} in waitForOpenNominations state", ignored)
        Behaviors.same
    }
  }

  private def receiveNominations(
      nominators: Set[DataTypes.Player],
      nominees: Set[DataTypes.Player],
      queuedNominations: Queue[DataTypes.Nomination]): Behavior[Command] =
    Behaviors.receiveMessage {
      case WrappedOpenNominations(_) =>
        messageActor ! SendPublicMessage(Messages.alreadyOpen)
        Behaviors.same

      case WrappedNominate(command) =>
        val TextChannelCommand.Nominate(nominee, metadata) = command

        val nominator  = metadata.sender
        val nomination = nominator -> nominee
        if (nominators.contains(nominator)) {
          messageActor ! SendPublicMessage(Messages.repeatNominator(nominator))
          Behaviors.same
        } else if (nominees.contains(nominee)) {
          messageActor ! SendPublicMessage(Messages.repeatNominee(nominee))
          Behaviors.same
        } else if (nominator == gameSetup.storyteller) {
          messageActor ! SendPublicMessage(Messages.storytellerNom)
          Behaviors.same
        } else {
          val nominationRegisteredMsg = Messages.registered(nomination)
          messageActor ! SendPublicMessage(nominationRegisteredMsg)
          messageActor ! SendPrivateChatMessage(nominationRegisteredMsg, gameSetup.storyteller)
          if (queuedNominations.isEmpty && context.child(ChildVotingActorName).isEmpty) {
            spawnVotingActor(nomination)
            receiveNominations(nominators + nominator, nominees + nominee, Queue.empty)
          } else {
            receiveNominations(
              nominators + nominator,
              nominees + nominee,
              queuedNominations.enqueue(nomination))
          }
        }

      case VotingComplete =>
        if (queuedNominations.isEmpty) {
          messageActor ! SendPublicMessage(Messages.noMoreQueued)
          Behaviors.same
        } else {
          val (nomination, newQueue) = queuedNominations.dequeue
          val nextNominationMsg      = Messages.nextQueuedNomination(nomination, newQueue.size)
          messageActor ! SendPublicMessage(nextNominationMsg)
          messageActor ! SendPrivateChatMessage(nextNominationMsg, gameSetup.storyteller)
          spawnVotingActor(nomination)
          receiveNominations(nominators, nominees, newQueue)
        }

      case WrappedCloseNominations(command) =>
        val TextChannelCommand.CloseNominations(timeout, metadata) = command
        if (metadata.sender != gameSetup.storyteller) {
          messageActor ! SendTextChannelMessage(Messages.notStoryteller, metadata.channel)
        } else {
          timers.startSingleTimer(CloseNominationsTimerKey, CloseNominationsTimeout, timeout)
          messageActor ! SendPublicMessage(Messages.closingSoon(timeout))
        }
        Behaviors.same

      case CloseNominationsTimeout =>
        messageActor ! SendPublicMessage(Messages.nominationsClosed)
        receptionist ! Unregister(nominateCommandActor)
        if (queuedNominations.isEmpty && context.children.isEmpty) {
          waitForOpenNominations()
        } else {
          closingNominations(queuedNominations)
        }
    }

  private def closingNominations(
      queuedNominations: Queue[DataTypes.Nomination]): Behavior[Command] =
    Behaviors.receiveMessage {
      case VotingComplete =>
        if (queuedNominations.isEmpty) {
          waitForOpenNominations()
        } else {
          val (nomination, newQueue) = queuedNominations.dequeue
          spawnVotingActor(nomination)
          closingNominations(newQueue)
        }

      case ignored =>
        context.log.warn("Ignoring command {} in closingNominations state", ignored)
        messageActor ! SendPublicMessage(Messages.closedAndWaiting(queuedNominations.size + 1))
        Behaviors.same
    }

  private def spawnVotingActor(nomination: DataTypes.Nomination): Unit = {
    val childName = ChildVotingActorName
    val votingActor =
      context.spawn(VotingActor(gameSetup, nomination, messageActor, receptionist), childName)
    context.watchWith(votingActor, VotingComplete)
  }
}
