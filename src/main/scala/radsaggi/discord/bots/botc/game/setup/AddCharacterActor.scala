package radsaggi.discord.bots.botc.game.setup

import akka.actor.typed.ActorRef
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.ActorContext
import akka.actor.typed.scaladsl.Behaviors
import radsaggi.discord.bots.botc.model._

object AddCharacterActor {
  type Command = PrivateChatCommand.AddCharacter

  /** An InternalEvent fired when the storyteller has added all the characters to the game. */
  case class CharactersAddedEvent(characters: DataTypes.Characters)

  private object Messages {
    val youAreNotStoryteller = "Trying to play storyteller? Maybe some other game, townie!"
    def added(n: Int)        = s"Received $n characters."

    def alreadyAdded(ch: DataTypes.Character) = s"Character '$ch' has already been added."
    def allAdded(characters: DataTypes.Characters) =
      s"Received all ${characters.size} characters:\n" + characters.map("  " + _).mkString("\n")
  }

  def apply(
      initData: GameData.Init,
      messageActor: ActorRef[ExternalMessage],
      replyTo: ActorRef[CharactersAddedEvent]): Behavior[Command] =
    Behaviors.setup(context =>
      new AddCharacterActor(context, initData, messageActor, replyTo).receive(Seq.empty))
}

import radsaggi.discord.bots.botc.game.setup.AddCharacterActor._

final class AddCharacterActor private (
    context: ActorContext[Command],
    initData: GameData.Init,
    messageActor: ActorRef[ExternalMessage],
    replyTo: ActorRef[CharactersAddedEvent]) {
  context.log.info("Starting AddCharacterActor")

  def receive(characters: Seq[DataTypes.Character]): Behavior[Command] =
    if (characters.size == initData.numPlayers) {
      context.log.info("All Characters have been added")
      messageActor ! SendPrivateChatMessage(Messages.allAdded(characters), initData.storyteller)
      replyTo ! CharactersAddedEvent(characters)
      Behaviors.stopped
    } else {
      Behaviors.receiveMessage { command =>
        val sender  = command.metadata.sender
        val channel = command.metadata.channel

        val newCharacter = command.character.toLowerCase.capitalize

        if (sender != initData.storyteller) { // AuthZ: Only storyteller can send these commands.
          context.log.warn("Received command AddCharacter from non storyteller sender {}", sender)
          messageActor ! SendPrivateChatMessage(Messages.youAreNotStoryteller, channel)
          Behaviors.same
        } else if (characters contains newCharacter) { // The chosen character already added
          context.log.warn("Received duplicate character to add from storyteller: {}", newCharacter)
          messageActor ! SendPrivateChatMessage(Messages.alreadyAdded(newCharacter), channel)
          Behaviors.same
        } else {
          messageActor ! SendPrivateChatMessage(Messages.added(characters.size + 1), channel)
          receive(characters :+ newCharacter)
        }
      }
    }
}
