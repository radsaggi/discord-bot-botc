package radsaggi.discord.bots.botc.game.setup

import akka.NotUsed
import akka.actor.typed.ActorRef
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.ActorContext
import akka.actor.typed.scaladsl.Behaviors
import radsaggi.discord.bots.botc.game.setup.AddCharacterActor.CharactersAddedEvent
import radsaggi.discord.bots.botc.game.setup.AllocationActor.CharactersAllocatedEvent
import radsaggi.discord.bots.botc.game.setup.JoinGameActor.PlayersJoinedEvent
import radsaggi.discord.bots.botc.game.receptionist.GameReceptionistActor.GameReceptionist
import radsaggi.discord.bots.botc.game.receptionist.GameReceptionistActor.Register
import radsaggi.discord.bots.botc.model._

object GameSetupActor {

  /** An InternalEvent fired when the game setup is complete. This is publicly visible. */
  case class GameSetupCompletedEvent(
      allocations: DataTypes.Allocation,
      seatingOrder: DataTypes.SeatedPlayers)

  object Messages {
    val setupComplete = "Game setup is now complete.!"
  }

  def apply(
      initData: GameData.Init,
      receptionist: GameReceptionist,
      messageActor: ActorRef[ExternalMessage],
      replyTo: ActorRef[GameSetupCompletedEvent]): Behavior[NotUsed] =
    Behaviors
      .setup[AnyRef](ctx =>
        new GameSetupActor(ctx, initData, messageActor, receptionist, replyTo).start())
      .narrow[NotUsed]
}

import radsaggi.discord.bots.botc.game.setup.GameSetupActor._

final class GameSetupActor private (
    context: ActorContext[AnyRef],
    initData: GameData.Init,
    messageActor: ActorRef[ExternalMessage],
    receptionist: GameReceptionist,
    replyTo: ActorRef[GameSetupCompletedEvent]) {
  context.log.info("Starting GameSetupActor for a new Game")

  /* Actor implementation behaviour methods. */

  def start(): Behavior[AnyRef] = {
    val joinGameActor =
      context.spawn(
        JoinGameActor(initData, messageActor, context.self.narrow[PlayersJoinedEvent]),
        "JoinGameActor")
    receptionist ! Register[TextChannelCommand.JoinGame](joinGameActor)

    val addCharacterActor =
      context.spawn(
        AddCharacterActor(initData, messageActor, context.self.narrow[CharactersAddedEvent]),
        "AddCharacterActor")
    receptionist ! Register[PrivateChatCommand.AddCharacter](addCharacterActor)

    Behaviors.receiveMessage {
      case PlayersJoinedEvent(players)      => playersJoined(players)
      case CharactersAddedEvent(characters) => charactersAdded(characters)
      case _                                => Behaviors.unhandled
    }
  }

  private def playersJoined(players: DataTypes.SeatedPlayers): Behavior[AnyRef] =
    Behaviors.receiveMessage {
      case CharactersAddedEvent(characters) => startAllocation(players, characters)
      case _                                => Behaviors.unhandled
    }

  private def charactersAdded(characters: DataTypes.Characters): Behavior[AnyRef] =
    Behaviors.receiveMessage {
      case PlayersJoinedEvent(players) => startAllocation(players, characters)
      case _                           => Behaviors.unhandled
    }

  private def startAllocation(
      players: DataTypes.SeatedPlayers,
      characters: DataTypes.Characters): Behavior[AnyRef] = {
    context.log.info("Starting character allocation")
    val allocationActor = context.spawn(
      AllocationActor(
        initData,
        players,
        characters,
        messageActor,
        context.self.narrow[CharactersAllocatedEvent]),
      "AllocationActor")
    receptionist ! Register[PrivateChatCommand.SelectCharacter](allocationActor)

    Behaviors.receiveMessage {
      case CharactersAllocatedEvent(allocation) =>
        context.log.info("Setup complete for players: {}", players)
        messageActor ! SendTextChannelMessage(Messages.setupComplete, initData.channels.publicText)
        replyTo ! GameSetupCompletedEvent(allocation, players)
        Behaviors.stopped

      case _ => Behaviors.unhandled
    }
  }
}
