package radsaggi.discord.bots.botc.game.receptionist

import akka.actor.typed.ActorRef
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.ActorContext
import akka.actor.typed.scaladsl.Behaviors
import com.typesafe.scalalogging.LazyLogging
import radsaggi.discord.bots.botc.model.ExternalCommand
import shapeless.HMap

import scala.reflect.ClassTag

/**
 * Actor responsible for forwarding {@link ExternalCommand}s to different game feature actors.
 *
 * <p> Whenever a new feature actor is spawned, it receives this receptionist actor and registers
 * itself to receive particular {@link ExternalCommand}s.
 *
 * <p>The Game top-level actor is responsible for spawning this receptionist and forwarding
 * {@link ExternalCommand}s to it.
 */
object GameReceptionistActor extends LazyLogging {
  type GameReceptionist = ActorRef[Command]

  sealed trait Command

  case class Deliver[C <: ExternalCommand](command: C) extends Command {
    private[receptionist] val classTag: ClassTag[C] = ClassTag(command.getClass)
    logger.trace("Created Deliver message for {} with ClassTag {}", this, classTag)
  }

  case class Register[C <: ExternalCommand](actor: ActorRef[C])(implicit tag: ClassTag[C])
      extends Command {
    logger.trace("Created Register message for {} with ClassTag {}", this, tag)
    private[receptionist] val classTag = tag
  }

  case class Unregister[C <: ExternalCommand](actor: ActorRef[C])(implicit tag: ClassTag[C])
      extends Command {
    logger.trace("Created Unregister message for {} with ClassTag {}", this, tag)
    private[receptionist] val classTag = tag
  }

  def apply(): Behavior[Command] =
    Behaviors.setup(ctx => new GameReceptionistActor(ctx).receive(HMap.empty))
}

import radsaggi.discord.bots.botc.game.receptionist.GameReceptionistActor._

final class GameReceptionistActor private (context: ActorContext[Command]) {
  // Type sorcery to allow HMap to contain values for the correct keys.
  private class CommandMapping[K, V]()

  private case class CommandKey[C <: ExternalCommand](classTag: ClassTag[C])
  implicit private def witness[C <: ExternalCommand]: CommandMapping[CommandKey[C], ActorRef[C]] =
    new CommandMapping[CommandKey[C], ActorRef[C]]

  /* Actor implementation behaviour methods. */

  def receive(registry: HMap[CommandMapping]): Behavior[Command] =
    Behaviors.receiveMessage {
      case command: Deliver[_]    => deliverCommand(registry, command)
      case command: Register[_]   => registerCommand(registry, command)
      case command: Unregister[_] => unregisterActor(registry, command)
    }

  private def deliverCommand[C <: ExternalCommand](
      registry: HMap[CommandMapping],
      command: Deliver[C]): Behavior[Command] = {
    registry.get(CommandKey[C](command.classTag)) match {
      case Some(actor) =>
        actor ! command.command
        Behaviors.same

      case None =>
        context.log.warn("Unhandled message received by GameReceptionist: {}", command)
        Behaviors.unhandled
    }
  }

  private def registerCommand[C <: ExternalCommand](
      registry: HMap[CommandMapping],
      command: Register[C]): Behavior[Command] = {
    val Register(actor) = command
    val key             = CommandKey[C](command.classTag)

    registry.get(key) match {
      case Some(actor) => context.unwatch(actor)
      case None        =>
    }

    context.log.debug("Receptionist adding actor: " + actor.path.name)
    context.watchWith(actor, Unregister[C](actor)(command.classTag))
    val commandEntry = key -> actor
    receive(registry + commandEntry)
  }

  private def unregisterActor[C <: ExternalCommand](
      registry: HMap[CommandMapping],
      command: Unregister[C]): Behavior[Command] = {
    val Unregister(actor) = command
    context.log.debug("Receptionist removed actor: " + actor.path.name)
    context.unwatch(actor)
    receive(registry - CommandKey[C](command.classTag))
  }
}
