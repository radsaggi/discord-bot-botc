package radsaggi.discord.bots.botc.game.conversations

import akka.actor.typed.ActorRef
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.ActorContext
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.scaladsl.TimerScheduler
import radsaggi.discord.bots.botc.game.receptionist.GameReceptionistActor.GameReceptionist
import radsaggi.discord.bots.botc.game.receptionist.GameReceptionistActor.Register
import radsaggi.discord.bots.botc.model
import radsaggi.discord.bots.botc.model.DataTypes.Message
import radsaggi.discord.bots.botc.model.DataTypes.Nomination
import radsaggi.discord.bots.botc.model.DataTypes.Player
import radsaggi.discord.bots.botc.model._

import scala.concurrent.duration.FiniteDuration

object VotingActor {

  sealed trait Command
  private case class WrappedStartVoting(command: TextChannelCommand.StartVoting) extends Command
  private case class WrappedVote(command: TextChannelCommand.Vote) extends Command
  private case object VoteTimeout extends Command

  private object VoteTimerKey

  private type VoteValue = Boolean
  private type VoteTally = Map[DataTypes.Player, VoteValue]

  private object Messages {
    val notStoryteller: Message = "You are not storyteller. Only the storyteller may do this."
    val notStarted: Message     = "Voting has not started yet. Storyteller will start this soon!"
    val alreadyStarted: Message = "Voting has already started.!"

    def startVoting(nomination: Nomination, players: Seq[Player]): Message =
      s"Ready to start voting for nomination ${DataTypes.toString(nomination)}" +
      "\nVoting order: " + players.map(_.printName).mkString(", ")

    def callForVote(nomination: Nomination, player: Player, timeout: FiniteDuration): Message =
      s"${player.printName} must now vote. " +
      s"Nomination: ${DataTypes.toString(nomination)}. " +
      s"You have $timeout left to vote."

    def vote(sender: Player, value: Boolean): Message =
      s"Registered vote from ${sender.printName}: ${DataTypes.toString(value)}"

    def earlyVote(sender: Player, value: Boolean): Message =
      s"Early vote from ${sender.printName}: ${DataTypes.toString(value)}. " +
      "This vote is not finalised yet, and may be changed as long as it is not your turn to vote."

    def alreadyVoted(sender: model.DataTypes.Player): Message =
      s"Discarding vote from ${sender.printName}. Their vote has already been registered."

    def timeoutReached(player: Player): Message =
      s"I am tired of this waiting. ${player.printName}'s vote automatically registered as NO!"

    def finaliseEarlyVote(player: Player, value: Boolean): Message =
      s"Finalising early vote from ${player.printName}: ${DataTypes.toString(value)}." +
      "This has now been registered."

    def complete(
        nomination: Nomination,
        ayeVoters: Iterable[Player],
        nayVoters: Iterable[Player]): Message = {
      s"Voting for nomination ${DataTypes.toString(nomination)} completed! ${ayeVoters.size} AYE; ${nayVoters.size} NAY" +
      "\n  All those who voted AYE: " + ayeVoters.map(_.printName).mkString(", ") +
      "\n  All those who voted NAY: " + nayVoters.map(_.printName).mkString(", ")
    }
  }

  def apply(
      gameSetup: GameData.Setup,
      nomination: Nomination,
      messageActor: ActorRef[ExternalMessage],
      receptionist: GameReceptionist): Behavior[Command] =
    Behaviors.setup { ctx =>
      Behaviors.withTimers { timer =>
        new VotingActor(ctx, timer, gameSetup, messageActor).start(receptionist, nomination)
      }
    }
}

import radsaggi.discord.bots.botc.game.conversations.VotingActor._

final class VotingActor private (
    context: ActorContext[Command],
    timers: TimerScheduler[Command],
    gameSetup: GameData.Setup,
    messageActor: ActorRef[ExternalMessage]) {
  context.log.debug("Starting VotingActor")

  @inline private def SendPublicMessage(message: Message): SendTextChannelMessage = {
    SendTextChannelMessage(message, gameSetup.channels.publicText)
  }

  private def buildVotingOrder(players: Seq[Player], pivot: Player): Seq[Player] = {
    val index = players.indexOf(pivot) + 1
    if (players.isDefinedAt(index)) {
      val (left, right) = players.splitAt(index)
      right ++ left
    } else {
      players
    }
  }

  def start(receptionist: GameReceptionist, nomination: Nomination): Behavior[Command] = {
    receptionist ! Register[TextChannelCommand.StartVoting](
      context.messageAdapter(WrappedStartVoting.apply))
    receptionist ! Register[TextChannelCommand.Vote](context.messageAdapter(WrappedVote.apply))

    waitForStartVoting(nomination, earlyVotes = Map.empty)
  }

  private def waitForStartVoting(nomination: Nomination, earlyVotes: VoteTally): Behavior[Command] =
    Behaviors.receiveMessage {
      case WrappedStartVoting(TextChannelCommand.StartVoting(voteTimeout, metadata)) =>
        if (metadata.sender != gameSetup.storyteller) {
          messageActor ! SendPublicMessage(Messages.notStoryteller)
          Behaviors.same
        } else {
          val votingOrder = buildVotingOrder(gameSetup.seatingOrder, nomination.nominee)
          messageActor ! SendPublicMessage(Messages.startVoting(nomination, votingOrder))
          val timeout: FiniteDuration =
            voteTimeout.getOrElse(gameSetup.gameConfig.defaultVoteTimeout)
          nextPlayer(timeout, nomination, earlyVotes, votingOrder, finalisedVotes = Map.empty)
        }

      case WrappedVote(TextChannelCommand.Vote(value, metadata)) =>
        messageActor ! SendPublicMessage(Messages.earlyVote(metadata.sender, value))
        waitForStartVoting(nomination, earlyVotes + (metadata.sender -> value))

      case _ =>
        messageActor ! SendPublicMessage(Messages.notStarted)
        Behaviors.same
    }

  private def nextPlayer(
      timeout: FiniteDuration,
      nomination: Nomination,
      earlyVotes: VoteTally,
      players: Seq[Player],
      finalisedVotes: VoteTally): Behavior[Command] =
    players match {
      case Nil =>
        val (ayeVotersMap, nayVotersMap) = finalisedVotes.partition(_._2)

        val votingCompleteMsg = Messages.complete(nomination, ayeVotersMap.keys, nayVotersMap.keys)
        messageActor ! SendPublicMessage(votingCompleteMsg)
        messageActor ! SendPrivateChatMessage(votingCompleteMsg, gameSetup.storyteller)
        Behaviors.stopped

      case currentPlayer :: remainingPlayers =>
        earlyVotes.get(currentPlayer) match {
          case None =>
            messageActor ! SendPublicMessage(
              Messages.callForVote(nomination, currentPlayer, timeout))
            timers.startSingleTimer(VoteTimerKey, VoteTimeout, timeout)

            def callback(newEarlyVotes: VoteTally, voteValue: VoteValue): Behavior[Command] = {
              val newVotes = finalisedVotes + (currentPlayer -> voteValue)
              nextPlayer(timeout, nomination, newEarlyVotes, remainingPlayers, newVotes)
            }
            collectVote(earlyVotes, currentPlayer, remainingPlayers.toSet, callback)

          case Some(voteValue) =>
            messageActor ! SendPublicMessage(Messages.finaliseEarlyVote(currentPlayer, voteValue))
            val newEarlyVotes     = earlyVotes - currentPlayer
            val newFinalisedVotes = finalisedVotes + (currentPlayer -> voteValue)
            nextPlayer(timeout, nomination, newEarlyVotes, remainingPlayers, newFinalisedVotes)
        }
    }

  def collectVote(
      earlyVotes: VoteTally,
      currentPlayer: Player,
      remainingPlayers: Set[DataTypes.Player],
      nextPlayerCallback: (VoteTally, VoteValue) => Behavior[Command]): Behavior[Command] =
    Behaviors.receiveMessage {
      case WrappedStartVoting(_) =>
        messageActor ! SendPublicMessage(Messages.alreadyStarted)
        Behaviors.same

      case WrappedVote(TextChannelCommand.Vote(value, metadata)) =>
        if (metadata.sender == currentPlayer) {
          timers.cancel(VoteTimerKey)
          messageActor ! SendPublicMessage(Messages.vote(metadata.sender, value))
          nextPlayerCallback(earlyVotes, value)
        } else if (remainingPlayers contains metadata.sender) {
          messageActor ! SendPublicMessage(Messages.earlyVote(metadata.sender, value))
          val updatedEarlyVotes = earlyVotes + (metadata.sender -> value)
          collectVote(updatedEarlyVotes, currentPlayer, remainingPlayers, nextPlayerCallback)
        } else {
          val sender = metadata.sender
          messageActor ! SendPublicMessage(Messages.alreadyVoted(sender))
          Behaviors.same
        }

      case VoteTimeout =>
        messageActor ! SendPublicMessage(Messages.timeoutReached(currentPlayer))
        nextPlayerCallback(earlyVotes, false)
    }
}
