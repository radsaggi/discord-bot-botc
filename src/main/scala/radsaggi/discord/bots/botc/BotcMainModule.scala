package radsaggi.discord.bots.botc

import akka.actor.typed.ActorRef
import akka.actor.typed.ActorSystem
import akka.actor.typed.Behavior
import com.softwaremill.macwire.wire
import com.softwaremill.macwire.wireWith
import net.dv8tion.jda.api.JDA
import pureconfig.ConfigSource
import radsaggi.discord.bots.botc.config.BotcBotConfig
import radsaggi.discord.bots.botc.jdaakka.BotcJdaWrapper
import radsaggi.discord.bots.botc.jdaakka.BotcRootActor
import radsaggi.discord.bots.botc.jdaakka.ExternalCommandParser
import radsaggi.discord.bots.botc.jdaakka.GameListener

final class BotcMainModule {
  // NOTE: This is not lazy because the config must be loaded for the module instance to be built.!
  val config: BotcBotConfig = ConfigSource.default.loadOrThrow[BotcBotConfig]

  lazy val jda: JDA = {
    val botcJDABuilder = wire[BotcJDABuilder]
    botcJDABuilder.build(config.botToken)
  }

  lazy val externalCommands: ExternalCommandParser       = wire[ExternalCommandParser]
  lazy val gameListener: GameListener                    = wire[GameListener]
  lazy val botcJdaWrapperFactory: BotcJdaWrapper.Factory = (jda: JDA) => wire[BotcJdaWrapper]

  private[this] lazy val botcRootActorBehaviour: Behavior[BotcRootActor.Command] =
    wireWith(BotcRootActor.apply _)
  lazy val botcRootActor: ActorRef[BotcRootActor.Command] =
    ActorSystem(botcRootActorBehaviour, "BotcBot")
}
