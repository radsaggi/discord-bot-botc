package radsaggi.discord.bots.botc.jdaakka

import akka.actor.typed.ActorRef
import akka.actor.typed.Behavior
import akka.actor.typed.LogOptions
import akka.actor.typed.scaladsl.ActorContext
import akka.actor.typed.scaladsl.Behaviors
import org.slf4j.event.Level
import radsaggi.discord.bots.botc.config.BotcBotConfig
import radsaggi.discord.bots.botc.game.BotcGameActor
import radsaggi.discord.bots.botc.model._

object BotcRootActor {
  // TODO: Fix logging configuration through application.conf.
  private val logOptions = LogOptions().withLevel(Level.DEBUG)

  sealed trait Command
  case class Initialise(jdaWrapper: BotcJdaWrapper) extends Command
  case class WrappedCommand(command: ExternalCommand) extends Command
  case class Terminate(postStop: () => Unit) extends Command

  private object Messages {
    def alreadyStarted(user: DataTypes.User) =
      s"Hey ${user.printName}! I can run only 1 game at a time; I am still learning!"
  }

  def apply(config: BotcBotConfig): Behavior[Command] =
    Behaviors.setup(context =>
      Behaviors.logMessages(logOptions, new BotcRootActor(context, config).receive()))
}

import radsaggi.discord.bots.botc.jdaakka.BotcRootActor._

final class BotcRootActor private (context: ActorContext[Command], config: BotcBotConfig) {
  type GameActorRef = ActorRef[ExternalCommand]

  def receive(): Behavior[Command] =
    Behaviors.receiveMessage {
      case Initialise(jdaWrapper) =>
        initialised(jdaWrapper)

      case Terminate(postStopRun) =>
        context.log.debug("Terminating BotcRootActor before initialisation.")
        Behaviors.stopped(postStopRun)

      case _ =>
        context.log.error("BotcRootActor needs to be initialised first.")
        Behaviors.same
    }

  private def initialised(jdaWrapper: BotcJdaWrapper): Behavior[Command] =
    Behaviors.receiveMessage {
      case Initialise(_) =>
        context.log.error("BotcRootActor has already been initialised.")
        Behaviors.same

      case WrappedCommand(externalCommand) =>
        externalCommand match {
          case newGame: TextChannelCommand.NewGame =>
            val gameActor =
              context.spawn(BotcGameActor(newGame, config, jdaWrapper), "BotcGameActor")
            gameStarted(jdaWrapper, gameActor)
          case _ =>
            Behaviors.same
        }

      case Terminate(postStopRun) =>
        Behaviors.stopped(postStopRun)
    }

  private def gameStarted(
      jdaWrapper: BotcJdaWrapper,
      gameActor: GameActorRef): Behavior[Command] = {
    Behaviors.receiveMessage {
      case Initialise(_) =>
        context.log.error("BotcRootActor has already been initialised.")
        Behaviors.same

      case WrappedCommand(externalCommand) =>
        externalCommand match {
          case newGameCommand: TextChannelCommand.NewGame =>
            context.log.warn(
              "Received command {} to start new game when one is already running",
              externalCommand)
            val sender  = newGameCommand.metadata.sender
            val channel = newGameCommand.metadata.channel
            jdaWrapper.sendMessage(SendTextChannelMessage(Messages.alreadyStarted(sender), channel))

          case _ => gameActor ! externalCommand
        }
        Behaviors.same

      case Terminate(postStopRun) =>
        context.log.warn("Terminating BotcRootActor before the game has ended.")
        Behaviors.stopped(postStopRun)
    }
  }
}
