package radsaggi.discord.bots.botc.jdaakka

import akka.actor.typed.ActorRef
import com.typesafe.scalalogging.LazyLogging
import fastparse.Parsed
import net.dv8tion.jda.api.events.ReadyEvent
import net.dv8tion.jda.api.events.ShutdownEvent
import net.dv8tion.jda.api.events.StatusChangeEvent
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import radsaggi.discord.bots.botc.config.BotcBotConfig
import radsaggi.discord.bots.botc.jdaakka.ExternalCommandParser.ParseFailureDetails
import radsaggi.discord.bots.botc.jdaakka.ExternalCommandParser.PrivateChatCommandBuilder
import radsaggi.discord.bots.botc.jdaakka.ExternalCommandParser.TextChannelCommandBuilder
import radsaggi.discord.bots.botc.model.SendPrivateChatMessage
import radsaggi.discord.bots.botc.model.SendTextChannelMessage

final class GameListener(
    config: BotcBotConfig,
    commandParser: ExternalCommandParser,
    botcJdaWrapperFactory: BotcJdaWrapper.Factory,
    botcRootActor: ActorRef[BotcRootActor.Command],
) extends ListenerAdapter
    with LazyLogging {

  private object Messages {
    val notACommand = "That is not a command. Commands typically start with a leading \"/\"."

    def parseFailed(failureDetails: ParseFailureDetails): String =
      "Command not understood. " +
      s"I expected ${failureDetails.expected}, but actually received '${failureDetails.found}'."
  }

  override def onPrivateMessageReceived(event: PrivateMessageReceivedEvent): Unit = {
    if (event.getAuthor.isBot) {
      return
    }

    logger.debug("Message Received on Private Channel: {}.", event.getMessage)
    val meta = JdaParser.newPrivateChannelMeta(event.getAuthor, event.getChannel)
    commandParser.parsePrivateChatCommand(event.getMessage) match {
      case parsed: Parsed.Success[PrivateChatCommandBuilder] =>
        val command = parsed.value(meta)
        logger.debug("Received PrivateChatCommand: {}", command)
        botcRootActor ! BotcRootActor.WrappedCommand(command)

      case failure: Parsed.Failure =>
        val details = ExternalCommandParser.failureDetails(failure)
        logger.debug("Parse failure for potential PrivateChatCommand:\n{}", details)
        val userMessage =
          if (details.maybeCommand) Messages.parseFailed(details) else Messages.notACommand
        botcJdaWrapperFactory(event.getJDA)
          .sendMessage(SendPrivateChatMessage(userMessage, meta.channel))
    }
  }

  override def onGuildMessageReceived(event: GuildMessageReceivedEvent): Unit = {
    if (event.getAuthor.isBot || event.getGuild.getIdLong != config.botGuildId) {
      return
    }

    logger.trace(
      "Message Received from Guild {} on Channel {} : {}.",
      event.getGuild,
      event.getChannel,
      event.getMessage
    )

    val meta =
      JdaParser.newTextChannelMeta(Some(event.getMember), event.getAuthor, event.getChannel)
    commandParser.parseTextChannelCommand(event.getMessage) match {
      case parsed: Parsed.Success[TextChannelCommandBuilder] =>
        val command = parsed.value(meta)
        logger.debug("Received TextChannelCommand: {}", command)
        botcRootActor ! BotcRootActor.WrappedCommand(command)

      case failure: Parsed.Failure =>
        val details = ExternalCommandParser.failureDetails(failure)
        if (details.maybeCommand) {
          logger.debug("Parse failure for potential TextChannelCommand:\n{}", details)
          botcJdaWrapperFactory(event.getJDA)
            .sendMessage(SendTextChannelMessage(Messages.parseFailed(details), meta.channel))
        }
    }
  }

  override def onStatusChange(event: StatusChangeEvent): Unit = {
    logger.trace("Status Change event: {} -> {}", event.getOldStatus, event.getNewStatus)
  }

  override def onReady(event: ReadyEvent): Unit = {
    botcRootActor ! BotcRootActor.Initialise(botcJdaWrapperFactory(event.getJDA))
    logger.info("BotcRootActor initialised: {}", botcRootActor)
  }

  override def onShutdown(event: ShutdownEvent): Unit = {
    logger.info("Terminating BotcRootActor: {}", botcRootActor)
    botcRootActor ! BotcRootActor.Terminate(() => logger.info("BotcRootActor terminated."))
  }
}
