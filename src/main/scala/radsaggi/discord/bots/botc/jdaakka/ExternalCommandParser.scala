package radsaggi.discord.bots.botc.jdaakka

import com.typesafe.scalalogging.LazyLogging
import fastparse.CharIn
import fastparse.CharsWhileIn
import fastparse.EagerOpsStr
import fastparse.End
import fastparse.P
import fastparse.Parsed
import fastparse.Parsed.TracedFailure
import fastparse.SingleLineWhitespace
import fastparse.Start
import fastparse.StringInIgnoreCase
import net.dv8tion.jda.api.entities.Message
import radsaggi.discord.bots.botc.model.DataTypes
import radsaggi.discord.bots.botc.model.PrivateChatCommand
import radsaggi.discord.bots.botc.model.PrivateChatCommand._
import radsaggi.discord.bots.botc.model.TextChannelCommand
import radsaggi.discord.bots.botc.model.TextChannelCommand._

import scala.concurrent.duration.DurationInt

/** Helper class for parsing commands from the messages received by the bot. */
//noinspection MutatorLikeMethodIsParameterless - to avoid IntelliJ requesting parameters for methods.
final class ExternalCommandParser extends LazyLogging {
  import ExternalCommandParser._

  private object Messages {
    val anyTextChannelCommand = "a known command (they are usually case-sensitive)"
    val numberOfPlayers       = "an integer number of players for the game"

    val anyPrivateChatCommand = "a known command (they are usually case-sensitive)"
    val characterName         = "a Character name (without spaces)"
    val characterSelection    = "an integer number for your choice of Character"
    val conversationChannels  = "an integer number of conversation voice channels to open"
  }

  def commandPrompt[_: P]: P[Unit] = P("/")

  // TextChannel Commands

  /** Parses a given message into an appropriate private chat command. */
  def parseTextChannelCommand(message: Message): Parsed[TextChannelCommandBuilder] =
    fastparse.parse(message.getContentRaw, textChannelCommand(_))

  private def textChannelCommand[_: P]: P[TextChannelCommandBuilder] =
    P(Start ~~ commandPrompt ~~/ anyTextChannelCommand(Messages.anyTextChannelCommand) ~ End)

  private def anyTextChannelCommand[_: P]: P[TextChannelCommandBuilder] =
    P(
      newGame | joinGame |
      enableConversations | disableConversations |
      openNominations | closeNominations | nominate | startVoting |
      vote | voteYesDirect | voteNoDirect)

  private def newGame[_: P]: P[TextChannelCommandBuilder] =
    P("NewGame" ~/ number(Messages.numberOfPlayers)).map(num => meta => NewGame(num, meta))

  private def joinGame[_: P]: P[TextChannelCommandBuilder] =
    P("JoinGame").map(_ => meta => JoinGame(meta))

  private def enableConversations[_: P]: P[TextChannelCommandBuilder] =
    P("StartConversations" ~/ number(Messages.conversationChannels).?).map(numChannels =>
      meta => EnableConversations(numChannels.getOrElse(7), meta))

  private def disableConversations[_: P]: P[TextChannelCommandBuilder] =
    P("StopConversations" ~/ number).map(duration =>
      meta => DisableConversations(duration.seconds, meta))

  private def openNominations[_: P]: P[TextChannelCommandBuilder] =
    P("OpenNominations").map(_ => meta => OpenNominations(meta))

  private def closeNominations[_: P]: P[TextChannelCommandBuilder] =
    P("CloseNominations" ~/ number).map(duration =>
      meta => CloseNominations(duration.seconds, meta))

  private def nominate[_: P]: P[TextChannelCommandBuilder] =
    P("Nominate" ~/ userMention).map(user => meta => Nominate(user, meta))

  private def startVoting[_: P]: P[TextChannelCommandBuilder] =
    P("StartVoting" ~/ number.?).map(optDuration =>
      meta => StartVoting(optDuration.map(_.seconds), meta))

  private def vote[_: P]: P[TextChannelCommandBuilder] =
    P("Vote" ~/ bool).map(value => meta => Vote(value, meta))

  private def voteYesDirect[_: P]: P[TextChannelCommandBuilder] =
    P(StringInIgnoreCase("yeah", "yes", "y", "aye").!).map(_ => meta => Vote(value = true, meta))

  private def voteNoDirect[_: P]: P[TextChannelCommandBuilder] =
    P(StringInIgnoreCase("nah", "no", "n", "nay").!).map(_ => meta => Vote(value = false, meta))

  // PrivateChat Commands

  /** Parses a given message into an appropriate private chat command. */
  def parsePrivateChatCommand(message: Message): Parsed[PrivateChatCommandBuilder] =
    fastparse.parse(message.getContentRaw, privateChatCommand(_))

  private def privateChatCommand[_: P]: P[PrivateChatCommandBuilder] =
    P(Start ~~ commandPrompt ~~/ anyPrivateChatCommand(Messages.anyPrivateChatCommand) ~ End)

  private def anyPrivateChatCommand[_: P]: P[PrivateChatCommandBuilder] =
    P(selectCharacter | addCharacter)

  private def selectCharacter[_: P]: P[PrivateChatCommandBuilder] =
    P("Select" ~/ number(Messages.characterSelection)).map(selection =>
      meta => SelectCharacter(selection, meta))

  private def addCharacter[_: P]: P[PrivateChatCommandBuilder] =
    P("AddRole" ~/ word(Messages.characterName)).map(character =>
      meta => AddCharacter(character, meta))
}

object ExternalCommandParser {
  type TextChannelCommandBuilder = TextChannelMeta => TextChannelCommand
  type PrivateChatCommandBuilder = PrivateChatMeta => PrivateChatCommand

  // Allow white-spaces only in a single line.
  implicit val whitespace: P[_] => P[Unit] = SingleLineWhitespace.whitespace

  private def number[_: P]: P[Int]      = P(CharIn("0-9").rep( /* min = */ 1).!.map(_.toInt))
  private def numberLong[_: P]: P[Long] = P(CharIn("0-9").rep( /* min = */ 1).!.map(_.toLong))
  private def word[_: P]: P[String]     = P(CharIn("a-zA-Z").rep( /* min = */ 1).!)
  private def userMention[_: P]: P[DataTypes.User] =
    P("<@" ~/ "!".? ~ numberLong ~ ">").map(userId => DataTypes.User(userId, None))

  private def bool[_: P]: P[Boolean] =
    P(word | CharsWhileIn("0-9", /* min  = */ 1).! | "+1".!).map(str =>
      str.toLowerCase match {
        case "yeah" => true
        case "yes"  => true
        case "aye"  => true
        case "y"    => true
        case "1"    => true
        case "+1"   => true
        case _      => false
      })

  def failureDetails(failure: Parsed.Failure) =
    new ParseFailureDetails(failure.trace(enableLogging = false))

  final class ParseFailureDetails private[ExternalCommandParser] (
      val tracedFailure: TracedFailure) {
    private val index: Int = tracedFailure.index

    lazy val maybeCommand: Boolean = index > 0
    lazy val expected: String      = tracedFailure.terminals.value.last()
    lazy val found: String         = Parsed.Failure.formatTrailing(tracedFailure.input, index)

    override def toString: String =
      s"""Details {
         |  expected: "$expected",
         |  found: $found,
         |  at: $index,
         |  stack: "${tracedFailure.stack.map { case (s, i) => s"$i:$s" }.mkString(" > ")}"
         |}""".stripMargin
  }
}
