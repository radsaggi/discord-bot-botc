package radsaggi.discord.bots.botc.jdaakka

import net.dv8tion.jda.api.entities.Category
import net.dv8tion.jda.api.entities.{Member => JdaMember}
import net.dv8tion.jda.api.entities.{PrivateChannel => JdaPrivateChannel}
import net.dv8tion.jda.api.entities.{TextChannel => JdaTextChannel}
import net.dv8tion.jda.api.entities.{User => JdaUser}
import net.dv8tion.jda.api.entities.{VoiceChannel => JdaVoiceChannel}
import radsaggi.discord.bots.botc.model.PrivateChatCommand.PrivateChatMeta
import radsaggi.discord.bots.botc.model.TextChannelCommand.TextChannelMeta
import radsaggi.discord.bots.botc.model._

/** Helper for converting JDA entities to models defined in DataTypes. */
object JdaParser {
  def newUser(user: JdaUser): DataTypes.User =
    DataTypes.User(user.getIdLong, Some(user.getAsMention))

  def newUser(member: JdaMember): DataTypes.User =
    DataTypes.User(member.getIdLong, Some(member.getAsMention))

  def newVoiceChannel(voiceChannel: JdaVoiceChannel): VoiceChannel =
    VoiceChannel(
      voiceChannel.getIdLong,
      voiceChannel.getName,
      Option(voiceChannel.getParent).map(newChannelCategory))

  def newPrivateChannel(privateChannel: JdaPrivateChannel): PrivateChannel =
    PrivateChannel(privateChannel.getIdLong, JdaParser.newUser(privateChannel.getUser))

  def newTextChannel(textChannel: JdaTextChannel): TextChannel =
    TextChannel(
      textChannel.getIdLong,
      textChannel.getName,
      Option(textChannel.getParent).map(newChannelCategory))

  def newTextChannelMeta(
      member: Option[JdaMember],
      user: JdaUser,
      channel: JdaTextChannel): TextChannelMeta =
    TextChannelMeta(member.map(newUser).getOrElse(newUser(user)), newTextChannel(channel))

  def newPrivateChannelMeta(user: JdaUser, channel: JdaPrivateChannel): PrivateChatMeta =
    PrivateChatMeta(newUser(user), newPrivateChannel(channel))

  def newChannelCategory(category: Category): ChannelCategory =
    ChannelCategory(category.getIdLong, category.getName)
}
