package radsaggi.discord.bots.botc.jdaakka

import java.util.concurrent.CompletableFuture
import java.util.concurrent.CompletionStage

import com.typesafe.scalalogging.LazyLogging
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.entities.Activity.ActivityType
import net.dv8tion.jda.api.entities.Activity
import net.dv8tion.jda.api.entities.Category
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import radsaggi.discord.bots.botc.config.BotcBotConfig
import radsaggi.discord.bots.botc.model.DataTypes.Message
import radsaggi.discord.bots.botc.model.DataTypes.User
import radsaggi.discord.bots.botc.model._

import scala.compat.java8.FutureConverters.toScala
import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.jdk.javaapi.CollectionConverters.asScala
import scala.language.implicitConversions
import scala.util.Try

/** Wrapper around JDA voice channel operations */
trait VoiceChannelsWrapper { futures: WithExecutionContext =>
  def guild: Guild

  def createParentCategory(channel: JdaChannel, name: String): Future[ChannelCategory] = {
    val jdaChannel = guild.getGuildChannelById(channel.id)

    val categoryFuture: Future[Category] = guild.createCategory(name).submit()
    categoryFuture.flatMap(jdaCategory => {
      val category = JdaParser.newChannelCategory(jdaCategory)
      jdaChannel.getManager.setParent(jdaCategory).submit().map(_ => category)
    })
  }

  def openVoiceChannel(category: ChannelCategory, name: String): Future[VoiceChannel] =
    guild
      .getCategoryById(category.id)
      .createVoiceChannel(name)
      .submit()
      .map(JdaParser.newVoiceChannel)

  def closeVoiceChannel(channel: VoiceChannel): Future[Unit] =
    guild.getVoiceChannelById(channel.id).delete().submit()

  def getMembersInVoiceChannel(channel: VoiceChannel): Iterable[User] =
    asScala(guild.getVoiceChannelById(channel.id).getMembers).map(JdaParser.newUser)

  def moveUserToChannel(user: User, toChannel: VoiceChannel): Future[Unit] = {
    val member  = guild.getMemberById(user.id)
    val channel = guild.getVoiceChannelById(toChannel.id)

    val memberFuture: Future[Member] =
      if (member != null) Future.successful(member)
      else guild.retrieveMemberById(user.id).submit()

    memberFuture.flatMap(member => guild.moveVoiceMember(member, channel).submit())
  }
}

/** Wrapper around JDA ExecutionContext for use in scala */
trait WithExecutionContext {
  implicit def executionContext: ExecutionContext

  def run[T](exec: ExecutionContext => Future[T]): Future[T] =
    Future.fromTry(Try(exec(executionContext))).flatten

  // Some implicit conversions for writing cleaner code

  implicit def javaFutureToScala[T]: CompletionStage[T] => Future[T] = toScala[T]
  implicit def voidToUnit(future: Future[Void]): Future[Unit]        = future.map(_ => ())
  implicit def voidFutureToScala(future: CompletableFuture[Void]): Future[Unit] =
    voidToUnit(toScala(future))
}

/** Wrapper around JDA api for ease of use by BotcRootActor. */
final class BotcJdaWrapper(config: BotcBotConfig, val jda: JDA)
    extends WithExecutionContext
    with VoiceChannelsWrapper
    with LazyLogging {

  override val executionContext: ExecutionContext =
    ExecutionContext.fromExecutorService(jda.getCallbackPool)

  override def guild: Guild = Option(jda.getGuildById(config.botGuildId)).get

  // TODO: handle the callbacks from the #queue() call.

  def sendMessage(message: ExternalMessage): Unit =
    message match {
      case SendPrivateChatMessage(message, channelOrUser) =>
        sendPrivateChatMessage(channelOrUser, message)

      case SendTextChannelMessage(message, channel) =>
        sendTextChannelMessage(channel, message)

      case UpdateStatusMessage(newStatus) =>
        jda.getPresence.setActivity(Activity.of(ActivityType.DEFAULT, newStatus))
    }

  def sendPrivateChatMessage(
      channelOrUser: Either[PrivateChannel, User],
      message: Message): Unit = {
    val (jdaChannel, userId) = channelOrUser match {
      case Left(channel) => (jda.getPrivateChannelById(channel.id), channel.user.id)
      case Right(user)   => (null, user.id)
    }
    if (jdaChannel != null) {
      jdaChannel.sendMessage(message).queue()
    } else {
      jda.openPrivateChannelById(userId).flatMap(_.sendMessage(message)).queue()
    }
  }

  def sendTextChannelMessage(channel: TextChannel, message: Message): Unit =
    jda.getTextChannelById(channel.id).sendMessage(message).queue()
}

object BotcJdaWrapper {
  type Factory = JDA => BotcJdaWrapper
}
