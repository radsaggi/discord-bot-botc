package radsaggi.discord.bots.botc.config

import pureconfig.ConfigReader
import pureconfig.generic.auto.exportReader
import pureconfig.generic.semiauto.deriveReader

import scala.concurrent.duration.Duration
import scala.concurrent.duration.FiniteDuration

case class ChannelConfig(
    categoryName: String,
    globalConversationsName: String,
    defaultConversationsCount: Int,
    conversationsNames: Seq[String]) {
  require(categoryName.nonEmpty, "channels.category-name must not be an empty string.")
  require(
    globalConversationsName.nonEmpty,
    "channels.global-conversations-name must not be an empty string.")
  require(
    defaultConversationsCount > 0,
    "channels.default-conversations-count must be a positive integer.")
  require(
    conversationsNames.nonEmpty,
    "channels.conversations-names should contain at least one name")
}

case class GameConfig(
    allowUserMultipleJoins: Boolean,
    defaultVoteTimeout: FiniteDuration,
    channels: ChannelConfig) {
  require(defaultVoteTimeout.isFinite, "vote-timeout should not be infinite!")
}

case class DebugConfig(autoShutdownTimeout: Option[Duration] = None)

case class BotcBotConfig(botToken: String, botGuildId: Long, debug: DebugConfig, game: GameConfig)

object BotcBotConfig {
  implicit val configReader: ConfigReader[BotcBotConfig] = deriveReader[BotcBotConfig]
}
