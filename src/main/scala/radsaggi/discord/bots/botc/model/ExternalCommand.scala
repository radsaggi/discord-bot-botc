package radsaggi.discord.bots.botc.model

import scala.concurrent.duration.FiniteDuration

/** Commands received by the Bot on JDA.  */
sealed trait ExternalCommand

/** Types of commands received by the bot on the public Text Channel. */
sealed trait TextChannelCommand extends ExternalCommand

object TextChannelCommand {
  // Game-Setup commands.
  case class NewGame(numPlayers: Int, metadata: TextChannelMeta) extends TextChannelCommand
  case class JoinGame(metadata: TextChannelMeta) extends TextChannelCommand

  // Game-conversation commands. For enabling / disabling private conversations.
  case class EnableConversations(numChannels: Int, metadata: TextChannelMeta)
      extends TextChannelCommand
  case class DisableConversations(timeout: FiniteDuration, metadata: TextChannelMeta)
      extends TextChannelCommand

  // Game-Nomination commands.
  case class OpenNominations(metadata: TextChannelMeta) extends TextChannelCommand
  case class CloseNominations(timeout: FiniteDuration, metadata: TextChannelMeta)
      extends TextChannelCommand
  case class StartVoting(voteTimeout: Option[FiniteDuration], metadata: TextChannelMeta)
      extends TextChannelCommand
  case class Nominate(user: DataTypes.User, metadata: TextChannelMeta) extends TextChannelCommand
  case class Vote(value: Boolean, metadata: TextChannelMeta) extends TextChannelCommand

  /** Additional info received by commands on a TextChannel */
  case class TextChannelMeta(sender: DataTypes.User, channel: TextChannel)
}

/** Types of commands received by the bot on private chat. */
sealed trait PrivateChatCommand extends ExternalCommand

object PrivateChatCommand {
  // Game-Setup commands.
  case class SelectCharacter(choice: Int, metadata: PrivateChatMeta) extends PrivateChatCommand
  case class AddCharacter(character: DataTypes.Character, metadata: PrivateChatMeta)
      extends PrivateChatCommand

  // Game-Nomination commands.
  case class MarkNomination(from: DataTypes.User, to: DataTypes.User, metadata: PrivateChatMeta)
      extends PrivateChatCommand

  /** Additional info received by commands on a Private Chat */
  case class PrivateChatMeta(sender: DataTypes.User, channel: PrivateChannel)
}
