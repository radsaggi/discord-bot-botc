package radsaggi.discord.bots.botc.model

trait JdaChannel {
  def id: Long
}

case class PrivateChannel(id: Long, user: DataTypes.User) extends JdaChannel with IdBasedType

case class VoiceChannel(id: Long, name: String, category: Option[ChannelCategory])
    extends JdaChannel
    with IdBasedType

case class TextChannel(id: Long, name: String, category: Option[ChannelCategory])
    extends JdaChannel
    with IdBasedType

case class ChannelCategory(id: Long, name: String) extends IdBasedType
