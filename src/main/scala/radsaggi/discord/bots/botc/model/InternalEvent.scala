package radsaggi.discord.bots.botc.model

/** Represents events fired internally within the Botc ActorSystem */

/** An InternalEvent fired when all the bot tasks have been completed. */
case object BotActionsCompleted

object InternalEvent
