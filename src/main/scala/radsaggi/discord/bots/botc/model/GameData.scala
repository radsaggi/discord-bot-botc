package radsaggi.discord.bots.botc.model

import radsaggi.discord.bots.botc.config.GameConfig
import radsaggi.discord.bots.botc.model.DataTypes.Allocation
import radsaggi.discord.bots.botc.model.DataTypes.SeatedPlayers
import radsaggi.discord.bots.botc.model.DataTypes.User

object GameData {

  /** Information available about the Game just after initialisation with a NewGame command. */
  case class Init(storyteller: User, numPlayers: Int, channels: Channels, gameConfig: GameConfig)

  /** Information about the game channels. */
  case class Channels(
      publicText: TextChannel,
      publicConversations: VoiceChannel,
      category: ChannelCategory)

  /** Information available about the Game just after setup is complete. */
  case class Setup(init: Init, allocations: Allocation, seatingOrder: SeatedPlayers) {
    @inline def storyteller: User      = init.storyteller
    @inline def channels: Channels     = init.channels
    @inline def gameConfig: GameConfig = init.gameConfig
  }
}
