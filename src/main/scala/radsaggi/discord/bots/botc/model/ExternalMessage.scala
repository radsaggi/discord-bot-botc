package radsaggi.discord.bots.botc.model

import radsaggi.discord.bots.botc.model.DataTypes.Message
import radsaggi.discord.bots.botc.model.DataTypes.User

sealed trait ExternalMessage

case class SendPrivateChatMessage(message: Message, channelOrUser: Either[PrivateChannel, User])
    extends ExternalMessage
case class SendTextChannelMessage(message: Message, channel: TextChannel) extends ExternalMessage
case class UpdateStatusMessage(newStatus: String) extends ExternalMessage

object SendPrivateChatMessage {
  def apply(message: Message, channel: PrivateChannel): SendPrivateChatMessage =
    new SendPrivateChatMessage(message, Left(channel))

  def apply(message: Message, user: DataTypes.User): SendPrivateChatMessage =
    new SendPrivateChatMessage(message, Right(user))
}
