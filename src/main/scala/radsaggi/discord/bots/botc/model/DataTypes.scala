package radsaggi.discord.bots.botc.model

object DataTypes {
  type Message = String

  final case class User(id: Long, mention: Option[String]) extends IdBasedType {
    def printName: String = mention.getOrElse(s"<@$id>")
  }

  // Game specific types.

  type Character     = String
  type Player        = User
  type SeatedPlayers = Seq[Player]
  type Characters    = Seq[Character]
  type Allocation    = Map[User, Character]
  type Nomination    = (User, User)

  @inline def toString(voteValue: Boolean): String =
    if (voteValue) "AYE" else "NAY"

  @inline def toString(nomination: Nomination): String =
    s"${nomination._1.printName} -> ${nomination._2.printName}"

  implicit class NominationOps(val underlying: (Player, Player)) extends AnyVal {
    @inline def nominator: Player = underlying._1
    @inline def nominee: Player   = underlying._2
  }
}

/** A trait that data-types can extend so that equality is based only on the `id` field. */
trait IdBasedType extends Equals {
  def id: Long

  // We need equality only on id.
  override def equals(anyThat: Any): Boolean =
    anyThat match {
      case that if this.canEqual(that) => that.asInstanceOf[IdBasedType].id == this.id
      case _                           => false
    }

  // We need equality only on id.
  override def hashCode(): Int = id.##
}
