package radsaggi.discord.bots.botc

import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.JDABuilder
import net.dv8tion.jda.api.requests.GatewayIntent
import net.dv8tion.jda.api.utils.ChunkingFilter
import net.dv8tion.jda.api.utils.cache.CacheFlag
import radsaggi.discord.bots.botc.config.BotcBotConfig
import radsaggi.discord.bots.botc.jdaakka.GameListener

import scala.jdk.javaapi.CollectionConverters.asJava

class BotcJDABuilder(config: BotcBotConfig, gameListener: GameListener) {

  private val INTENTS = List(
    GatewayIntent.DIRECT_MESSAGES,
    GatewayIntent.DIRECT_MESSAGE_REACTIONS,
    GatewayIntent.GUILD_MEMBERS,
    GatewayIntent.GUILD_VOICE_STATES,
    GatewayIntent.GUILD_MESSAGES,
    GatewayIntent.GUILD_MESSAGE_REACTIONS,
  )

  private val DISABLED_CACHES = List(
    CacheFlag.EMOTE,
  )

  def build(botToken: String): JDA =
    JDABuilder
      .createDefault(botToken, asJava(INTENTS))
      .setChunkingFilter(ChunkingFilter.include(config.botGuildId))
      .disableCache(asJava(DISABLED_CACHES))
      .addEventListeners(gameListener)
      .build()
}
