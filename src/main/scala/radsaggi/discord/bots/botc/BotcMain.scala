package radsaggi.discord.bots.botc

object BotcMain extends App {
  // TODO: Fix logging configuration through application.conf.
  val LOG_LEVEL = "DEBUG"
  System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", LOG_LEVEL)

  val botcMainModule = new BotcMainModule

  botcMainModule.jda.awaitReady()
  Console.println("Botc JDA Bot is now ready and online.")

  // TODO: Replace with a better shutdown mechanism.
  botcMainModule.config.debug.autoShutdownTimeout.foreach { timeout =>
    Thread.sleep(timeout.toMillis)
    Console.println("Triggering auto shutdown")
    botcMainModule.jda.shutdown()
  }
}
