## scalafmt setup
version = "2.5.0-RC1"
project.git = true

## Formatting config

style = defaultWithAlign

align {
  tokens = [
    {code = "->"},
    {code = "<-"},
    {code = "=", owner = "(Enumerator.Val|Defn.(Va(l|r)|Def|Type))"},
    {code = "=>", owner = "Case"}
  ]
}

assumeStandardLibraryStripMargin = true

danglingParentheses = false

# http://docs.scala-lang.org/style/scaladoc.html recommends the JavaDoc style.
# scala/scala is written that way too https://github.com/scala/scala/blob/v2.12.2/src/library/scala/Predef.scala
docstrings = JavaDoc

# For better code clarity
#danglingParentheses = true

indentOperator = spray

maxColumn = 100

newlines {
  afterCurlyLambda = squash

  topLevelStatements = [before]

  topLevelStatementsMinBreaks = 2

  penalizeSingleSelectMultiArgList = false
}

rewrite {
  rules = [AvoidInfix, ExpandImportSelectors, RedundantParens, SortModifiers]

  redundantBraces.stringInterpolation = true

  sortModifiers.order = [
    "implicit", "final", "sealed", "abstract", "override", "private", "protected", "lazy"
  ]
}

spaces.inImportCurlyBraces = false

trailingCommas = preserve

unindentTopLevelOperators = true
